var $ = {
    new: function (node) { return document.createElement(node); },
    q: function(query) { return document.querySelectorAll(query); },
    q1: function(query) { return document.querySelector(query); },
    id: function(id) { return document.getElementById(id); },
};

var CONFIG = {
    smallestWidth: 5,
    threadWidth: 10,
    speed: 200,
};

var PATTERNS = [
    { 
        name: "basic",
        data: {
            threading: [ null, [1], [2] ],
            tieup: [ null, [1], [2] ],
            treadling: [ null, [1], [2] ], 
        }
    }, {
        name: "1-2",
        data: {
            threading: [ null, [1], [2], [3] ],
            tieup: [ null, [2, 3], [1, 3], [1, 2] ],
            treadling: [ null, [1], [2], [3] ],
        },
    }, {
        name: "2-2 chevron",
        data: {
            threading: [ null, [1], [2], [3], [4] ],
            tieup: [ null, [2, 3], [3, 4], [1, 4], [1, 2] ],
            treadling: [ null, [1], [2], [3], [4] ],
        },
    }, {
        name: "s diagonal",
        data: {
            threading: [ null, [1], [2], [3], [4], [2], [5] ],
            tieup: [ null, [2, 3, 5], [1, 3], [1, 2, 4], [2, 3, 5], [4, 5], [1, 2, 4] ],
            treadling: [ null, [1], [2], [3], [4], [5], [6] ],
        },
    }, {
        name: "stripes-zigsags",
        data: {
            threading: [ null, [1], [2], [3], [4], [2], [3], [2], [4], [3], [2], [1] ],
            tieup: [ null, [1, 2], [2, 3], [3, 4], [4, 1] ],
            treadling: [ null, [1], [2], [3], [4] ],
        },
    }, {
        name: "triangles",
        data: {
            threading: [ null, [1], [2], [3], [4], [3], [2] ],
            tieup: [ null, [1, 2], [2, 3], [1, 4], [3, 4] ],
            treadling: [ null, [1], [2], [3], [4], [2], [3] ],
        },
    }, {
        name: "s",
        data: {
            threading: [ null, [2], [1], [1], [3] ],
            tieup: [ null, [1], [2], [3] ],
            treadling: [ null, [1], [2], [1], [3] ],
        },
    }, {
        name: "corrupted zigzag",
        data: {
            threading: [ null, [1], [2], [3], [4] ],
            tieup: [ null, [4], [2, 3, 4], [2], [1, 2, 4] ],
            treadling: [ null, [1], [2], [3], [4] ],
        },
    }, {
        name: "double diamond",
        data: {
            threading: [ null, [1], [2], [3], [4], [1], [2], [3], [4], [3], [2], [1], [4], [3], [2] ],
            tieup: [ null, [1, 4], [1, 2], [2, 3], [3, 4] ],
            treadling: [ null, [1], [2], [3], [4], [1], [2], [3], [4], [3], [2], [1], [4], [3], [2] ],
        },
    }, {
        name: "offset stars",
        data: {
            threading: [ null, [1], [2], [3], [4] ],
            tieup: [ null, [1], [2], [3], [4] ],
            treadling: [ null, [1], [3], [2], [4] ],
        },
    }, {
        name: "reverse-offset",
        data: {
            threading: [ null, [1], [2], [3], [4] ],
            tieup: [ null, [2, 3, 4], [1, 3, 4], [1, 2, 4], [1, 2, 3] ],
            treadling: [ null, [1], [4], [2], [3] ],
        }
    }, {
        name: "messy",
        data: {
            threading: [ null, [1], [2], [3], [4] ],
            tieup: [ null, [2, 4], [2, 3], [1, 4], [1, 3] ],
            treadling: [ null, [1], [2], [3], [1], [4], [3], [2], [4] ],
        },
    }
];

var UPLOADED_PATTERNS = {

};

function randomInRange (min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function getPattern () {
    var uploaded = Object.values(UPLOADED_PATTERNS);
    var i = randomInRange(0, PATTERNS.length + uploaded.length);

    return i < PATTERNS.length
        ? PATTERNS[i]
        : uploaded[i - PATTERNS.length];
}

function createWeave (wif) {

    var fitWindow = Math.ceil(window.innerWidth / CONFIG.threadWidth);
    var width = fitWindow;
    var length = randomInRange(50, 100);

    if (wif == null) {
        var template = Object.assign({}, getPattern().data);
        // Pick Random Colors
        template.color_table = {};
        if (template.color_palette == null) {
            template.color_palette = { entries: 2 };
        }
        for (var i = 1; i <= template.color_palette.entries; i++) {
            template.color_table[i] = [randomInRange(0, 254), randomInRange(0, 254), randomInRange(0, 254)];
        }
        if (template.warp_colors == null) {
            template.warp_colors = [null, 1];
        }
        if (template.weft_colors == null) {
            template.weft_colors = [null, 2];
        }
        wif = new WIF(template);
    }

    currentWif = wif;

    return {
        length: length,
        width: width,
        wif: wif,
        current: {
            length: 0,
            treadle: 0,
            line: null,
            face: null,
        }, previous: {
            length: 0,
            treadle: 0,
            line: null,
            face: null,
        }
    };
}



function advance (weave) {
    var line = [];
    var face = [];

    weave.previous.length = weave.current.length;
    weave.previous.treadle = weave.previous.treadle;
    weave.previous.line = weave.current.line;
    weave.previous.face = weave.current.face;

    var data = weave.wif.data;
    var treadles = data.treadling[data.treadling.length - weave.current.treadle - 1];
    var shafts = treadles.reduce(function(current, value) { return current.concat(data.tieup[value]) }, [])
    for (var i = 0; i < weave.width; i++) {
        var threadingIndex = (i % (data.threading.length - 1)) + 1;
        var isShowingWarp = data.threading[threadingIndex].reduce(
            function (current, value) {
                return current || shafts.includes(value)
            }, false);

        var warpColorIndex = (i % (data.warp_colors.length - 1)) + 1;
        var warpColor = data.color_table[data.warp_colors[warpColorIndex]];

        var weftColorIndex = (weave.current.length % (data.weft_colors.length - 1)) + 1;
        var weftColor = data.color_table[data.weft_colors[weftColorIndex]];

        face.push(isShowingWarp);
        line.push(isShowingWarp
            ? warpColor
            : weftColor);
    }

    var countTreadles = data.treadling.length - 1;
    weave.current.treadle = (weave.current.treadle + 1) % countTreadles;


    weave.current.length = weave.current.length + 1;
    weave.current.line = line;
    weave.current.face = face;

    return weave;
}

function resizeCanvas ($canvas) {
    if ($canvas.width != window.innerWidth || $canvas.height != window.innerHeight) {
        var $newCanvas = $.new("canvas");
        $newCanvas.id = "loom";
        $newCanvas.classList.add("d-block");
        $newCanvas.width = window.innerWidth;
        $newCanvas.height = window.innerHeight;
        $newCanvas.getContext("2d", { willReadFrequently: true }).drawImage($canvas, 0, 0);
        $canvas.parentNode.replaceChild($newCanvas, $canvas);
        $canvas = $newCanvas;
    }
    return $canvas;
}

var PIXEL_TRANSPARENT = [0, 0, 0, 0];
var PIXEL_THREAD_OVERLAP = [0, 0, 0, 254];

function drawLine (imageData, line, currentFace, previousFace) {
    // 4 bytes per pixel, for the whole width, for each thickness of the thread
    var dataWidth = imageData.width * 4 * CONFIG.threadWidth;
    var dividerWidth = imageData.width * 4 * (CONFIG.threadWidth - 1);

    var pixelColumn = 0;
    var pixelBox = 0;
    var lineIndex = 0;
    var rgb, showWarpDivider, showWeftDivider;

    for (var i = 0; i < dataWidth; i += 4) {

        rgb = PIXEL_TRANSPARENT;
        if (lineIndex < line.length) {

            showWarpDivider = pixelColumn % CONFIG.threadWidth == 0
                && lineIndex > 0
                // previous thread was showing a different face to this one
                && (currentFace[lineIndex] != currentFace[lineIndex - 1]
                    // showing warp and previous thread was also showing warp
                    || (currentFace[lineIndex] && currentFace[lineIndex - 1]));

            // On last row of pixels because previous line is displayed under this line
            showWeftDivider = i >= dividerWidth
                && (previousFace == null
                // previous line was showing a different face to this one
                || (currentFace[lineIndex] != previousFace[lineIndex]
                    // showing weft and previous line was also showing weft
                    || (!currentFace[lineIndex] && !previousFace[lineIndex])));


            if (showWarpDivider || showWeftDivider) {
                rgb = PIXEL_THREAD_OVERLAP;
            } else {
                rgb = line[lineIndex];
            }
        }

        imageData.data[i + 0] = rgb[0];
        imageData.data[i + 1] = rgb[1];
        imageData.data[i + 2] = rgb[2];
        imageData.data[i + 3] = rgb[3] || 254;

        pixelBox = (pixelBox + 1) % CONFIG.threadWidth;
        if (pixelBox == 0) {
            lineIndex += 1;
        }
        pixelColumn += 1;
        if (pixelColumn == imageData.width) {
            pixelColumn = 0;
            lineIndex = 0;
            pixelBox = 0;
        }
    }
}

function printLine (line, currentFace, previousFace) {
    var $canvas = $.id("loom");
    $canvas = resizeCanvas($canvas);
    var context = $canvas.getContext("2d", { willReadFrequently: true });

    // Shift the previous data down
    var previousData = context.getImageData(0, 0, $canvas.width, $canvas.height - CONFIG.threadWidth);
    context.putImageData(previousData, 0, CONFIG.threadWidth);

    // Draw the new line up top
    var newData = context.getImageData(0, 0, $canvas.width, CONFIG.threadWidth);
    drawLine(newData, line, currentFace, previousFace);
    context.putImageData(newData, 0, 0);
}

var currentLoop = null;
var currentInterval = null;
var currentWif = null;

function loop (weave) {
    if (weave.current.length >= weave.length) {
        weave = threadLoom();
    }

    weave = advance(weave);

    printLine(weave.current.line, weave.current.face, weave.previous.face);
}

function start () {
    if (currentInterval == null) {
        currentInterval = setInterval(currentLoop, CONFIG.speed);
        $.q1("button.play .bi-pause-fill").classList.remove("d-none");
        $.q1("button.play .bi-play-fill").classList.add("d-none");
    }
}

function stop () {
    if (currentInterval != null) {
        clearInterval(currentInterval)
        currentInterval = null;
        $.q1("button.play .bi-pause-fill").classList.add("d-none");
        $.q1("button.play .bi-play-fill").classList.remove("d-none");
    }
}

function restart () {
    if (currentInterval != null) {
        stop();
        start();
    }
}

function threadLoom (wif) {
    stop();

    var weave = createWeave(wif);

    currentLoop = loop.bind(this, weave)
    start();

    return weave;
}

var CLICK_HANDLERS = {
    "play": function () {
        if (currentInterval == null) {
            start();
        } else {
            stop();
        }
    },
    "next": function () {
        threadLoom();
    },
    "faster": function () {
        if (CONFIG.speed > 100) {
            CONFIG.speed = Math.max(100, CONFIG.speed - 100);
            if (CONFIG.speed == 100) {
                $.q1("button.faster").classList.add("disabled");
            }
        }
        restart();
    },
    "slower": function () {
        CONFIG.speed += 100;
        $.q1("button.faster").classList.remove("disabled");
        restart();
    },
    "larger": function () {
        CONFIG.threadWidth += 1;
        $.q1("button.smaller").classList.remove("disabled");
    },
    "smaller": function () {
        if (CONFIG.threadWidth > 1) {
            CONFIG.threadWidth = Math.max(CONFIG.smallestWidth, CONFIG.threadWidth - 1);
            if (CONFIG.threadWidth == CONFIG.smallestWidth) {
                $.q1("button.smaller").classList.add("disabled");
            }
        }
    },
    "download": function () {
        if (currentWif == null) {
            return;
        }
        var filename = "weave.wif";
        var $download = $.new("a");
        $download.classList.add("d-none");
        var blob = new Blob([currentWif.toString()], { type: "text/plain" });
        var url = window.URL.createObjectURL(blob);
        $download.href = url;
        $download.download = filename;
        $download.click();
        window.URL.revokeObjectURL(url);
    },
    "upload": function () {
        $.id("upload").click();
    },
    "show-draft": function () {
        stop();

        new Draft(currentWif, {
            drawdownWidth: 64,
            drawdownHeight: 64,
        }).initialize();

        $.q1(".loom-view").classList.add("d-none");
        $.q1(".draft-view").classList.remove("d-none");
    },
    "quit-draft": function () {
        $.q1(".draft-view").classList.add("d-none");
        $.q1(".loom-view").classList.remove("d-none");
    },
};

var CHANGE_HANDLERS = {
    "upload": function (ev) {
        ev.preventDefault();

        var files = (ev.dataTransfter ? ev.dataTransfter : ev.target).files;
        if (files.length) {
            var file = files[0];
            WIF.read(file).then(function (wif) {
                UPLOADED_PATTERNS[file.name] = wif;
                threadLoom(wif);
            }).finally(function () {
                // Reset the input field to allow uploading the same file again
                ev.target.value = "";
            });
        }

        return false;
    },
};

function attachHandlers () {
    var elements = $.q("[handler-click]");
    for (var i = 0; i < elements.length; i++) {
        var handler = elements[i].getAttribute("handler-click");
        elements[i].addEventListener("click", CLICK_HANDLERS[handler]);
    }

    elements = $.q("[handler-change]");
    for (var i = 0; i < elements.length; i++) {
        var handler = elements[i].getAttribute("handler-change");
        elements[i].addEventListener("change", CHANGE_HANDLERS[handler]);
    }
}

function ready (fn) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

ready(function () {
    attachHandlers();

    threadLoom();
});
