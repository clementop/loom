function Canvas (options) {
    this.options = Object.assign({
        $target: null,
        boxSide: null,
        width: null,
        height: null,
        render: null,
    }, options);

    this.initialize = function initialize () {
        this.width = this.options.boxSide * this.options.width;
        this.height = this.options.boxSide * this.options.height;

        this.$canvas = $.new("canvas");
        this.$canvas.setAttribute("width", this.width);
        this.$canvas.setAttribute("height", this.height);
        this.$canvas.classList.add("d-block");

        this.context = this.$canvas.getContext("2d");

        this.render();

        this.options.$target.replaceChildren(this.$canvas);
    }.bind(this);

    this.drawBox = function drawBox (x, y, color) {
        this.context.fillStyle = rgb(color);
        this.context.fillRect(x * this.options.boxSide, y * this.options.boxSide,
            this.options.boxSide, this.options.boxSide);
    }.bind(this);

    this.lineTo = function lineTo (x, y, visible) {
        visible ? this.context.lineTo(x, y) : this.context.moveTo(x, y);
    }.bind(this);

    this.drawBorderAround = function drawBorderAround (x, y, width, height, sides) {
        x = x * this.options.boxSide;
        y = y * this.options.boxSide;
        width = width * this.options.boxSide;
        height = height * this.options.boxSide;
        sides = sides || { top: true, right: true, bottom: true, left: true };

        this.context.lineWidth = 1;
        this.context.strokeStyle = "black"
        this.context.beginPath();
        this.context.moveTo(x, y);
        this.lineTo(x + width, y, sides.top);
        this.lineTo(x + width, y + height, sides.right);
        this.lineTo(x, y + height, sides.bottom);
        this.lineTo(x, y, sides.left);
        this.context.stroke();
    }.bind(this);

    this.drawBorder = function drawBorder (x, y, sides) {
        this.drawBorderAround(x, y, 1, 1, sides);
    }.bind(this);

    this.drawBorderedBox = function drawBorderedBox (x, y, color) {
        this.drawBox(x, y, color);
        this.drawBorder(x, y);
    }.bind(this);

    this.drawCharacter = function drawCharacter (x, y, character, color) {
        x = (x * this.options.boxSide) + Math.floor(this.options.boxSide / 2);
        y = (y * this.options.boxSide) + Math.floor(this.options.boxSide / 2);

        this.context.fillStyle = color || "black";
        this.context.font = this.options.boxSide + "px courier";
        this.context.textAlign = "center";
        this.context.textBaseline = "middle";
        this.context.fillText(character, x, y);
    }.bind(this);

    this.render = function render () {
        this.options.render();
    }.bind(this);

    function rgb (color) {
        return "rgb(" + color.join(",") + ")";
    }

    return this;
}