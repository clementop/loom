// wif.js
// Work with files following the Weaving Information File specification.

var WIF_SECTION = /\[(?<name>[^\]]+)\]/;
var WIF_COMMENT = /^;.*$/;
var WIF_KEYLINE = /^(?<key>[^=]+)=(?<value>.*)$/;

function internal (key) {
    return key.toLowerCase().replaceAll(" ", "_");
}

function externalSection (key) {
    return key.toUpperCase().replaceAll("_", " ");
}

function externalKeyline (key) {
    return key.split("_").map(function (word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }).join(" ");
}

function stripComment (value) {
    return value.split(";", 1)[0].trim();
}

function WIF (data) {
    this.data = data || {};

    this.accept = function (section, key, value) {
        section = internal(section);
        key = internal(key);

        var typeKey = Number.isInteger(parseInt(key)) ? "n" : key;

        var dataType = (WIF_DATATYPES[section]||{})[typeKey];

        if (dataType) {
            if (dataType === "float") {
                value = parseFloat(stripComment(value));
            } else if (dataType === "boolean") {
                value = ["true", "on", "yes", "1"].includes(stripComment(value));
            } else if (dataType === "integer") {
                value = parseInt(stripComment(value));
            } else if (dataType === "integer_arr") {
                value = stripComment(value)
                    .split(",")
                    .map(function (i) { return parseInt(i); });
            }

            if (this.data[section] === undefined) {
                this.data[section] = typeKey === "n" ? [ null ] : {};
            }
            this.data[section][key] = value;
        }
    }.bind(this);

    this.toString = function () {
        var file = "";
        var sections = Object.keys(this.data);
        for (var i = 0; i < sections.length; i++) {
            var section = sections[i];
            file += "[" + externalSection(section) + "]" + "\n";
            var keylines = Object.keys(this.data[section]);
            for (var j = 0; j < keylines.length; j++) {
                var key = keylines[j];
                var value = this.data[section][key];
                if (value != null) {
                    file += externalKeyline(key) + "=" + value + "\n";
                }
            }
        }
        return file;
    }.bind(this);

    return this;
}

function parseWifFile (contents) {
    var lines = contents.split(/(?:\r\n|\r|\n)/g);
    var sectionMatch, commentMatch, keylineMatch;
    var section, key, value, valueNoComment;

    var wif = new WIF();

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i].trim();

        var sectionMatch = line.match(WIF_SECTION);
        if (sectionMatch) {
            section = sectionMatch.groups.name;
            continue;
        }

        var commentMatch = line.match(WIF_COMMENT);
        if (commentMatch) {
            continue;
        }

        var keylineMatch = line.match(WIF_KEYLINE);
        if (keylineMatch) {
            key = keylineMatch.groups.key.trim();
            value = keylineMatch.groups.value.trim();
            wif.accept(section, key, value);
        }
    }

    return wif;
}

function readWifFile (file) {
    return new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.onload = function () {
            try {
                var wif = parseWifFile(this.result);
                resolve(wif);
            } catch (error) {
                reject(error);
            }
        }
        reader.onerror = function () {
            reject(this.error);
        }
        reader.readAsText(file);
    });
}

var WIF_DATATYPES = {
    wif: {
        version: "float",
        date: "string",
        developers: "string",
        source_program: "string",
        source_version: "string",
    }, contents: {
        color_palette: "boolean",
        weaving: "boolean",
        warp: "boolean",
        weft: "boolean",
        tieup: "boolean",
        color_table: "boolean",
        threading: "boolean",
        warp_colors: "boolean",
        treadling: "boolean",
        weft_colors: "boolean",
    }, color_palette: {
        entries: "integer",
        range: "integer_arr",
    }, weaving: {
        shafts: "integer",
        treadles: "integer",
    }, warp: {
        threads: "integer",
        color: "integer_arr",
    }, weft: {
        threads: "integer",
        color: "integer_arr",
    }, tieup: {
        n: "integer_arr",
    }, color_table: {
        n: "integer_arr",
    }, threading: {
        n: "integer_arr",
    }, warp_colors: {
        n: "integer",
    }, treadling: {
        n: "integer_arr",
    }, weft_colors: {
        n: "integer",
    }
};

WIF.read = readWifFile;