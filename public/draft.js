var COLOR_TABLE = [
    [0, 0, 0, 0], // transparent
    [0, 0, 0, 254], // black
    [254, 254, 254, 254], // white
];

function Draft(wif, options) {
    this.wif = wif;
    this.options = Object.assign({
        $target: document.querySelector(".draft-target"),
        drawdownWidth: wif.data.threading.length - 1,
        drawdownHeight: wif.data.treadling.length - 1,
        boxSide: 10,
    }, options);

    this.initialize = function initialize () {
        this.options = Object.assign(this.options, {
            treadleCount: maxIn(wif.data.treadling),
            shaftCount: maxIn(wif.data.threading),
        });

        var extraSpace = 5; // 1 box wrapper (2) + space after colors + space after treadling
        this.width = this.options.drawdownWidth + this.options.treadleCount + extraSpace;
        this.height = this.options.drawdownHeight + this.options.shaftCount + extraSpace;

        this.canvas = new Canvas({
            $target: this.options.$target,
            boxSide: this.options.boxSide,
            width: this.width,
            height: this.height,
            render: this.render,
        });

        this.offset = {};
        this.offset.xCenter = this.options.drawdownWidth + 1;
        this.offset.yCenter = this.options.shaftCount + 3;
        this.offset.xWeft = this.offset.xCenter + this.options.treadleCount + 2;
        this.offset.yWarp = 1;

        this.canvas.initialize();
    };

    this.renderThreading = function renderThreading () {
        for (var col = 1; col <= this.options.drawdownWidth; col++) {
            var threads = this.wif.data.threading[((col - 1) % (this.wif.data.threading.length - 1)) + 1];

            for (var shaft = 1; shaft <= this.options.shaftCount; shaft++) {
                this.canvas.drawBorderedBox(this.offset.xCenter - col, this.offset.yCenter - shaft,
                    COLOR_TABLE[threads.includes(shaft) ? 1 : 2]);
            }
        }

    }.bind(this);

    this.renderWarp = function renderWarp () {
        for (var col = 1; col <= this.options.drawdownWidth; col++) {
            this.canvas.drawBorderedBox(this.offset.xCenter - col, this.offset.yWarp,
                this.wif.data.color_table[this.wif.data.warp_colors[1]]);
        }

    }.bind(this);

    this.renderTieup = function renderTieup () {
        for (var row = 1; row <= this.options.shaftCount; row++) {
            for (var col = 1; col <= this.options.treadleCount; col++) {
                this.canvas.drawBorderedBox(
                    this.offset.xCenter + col, this.offset.yCenter - row,
                    COLOR_TABLE[this.wif.data.tieup[col].includes(row) ? 1 : 2]);
            }
        }

        for (var shaft = 1; shaft <= this.options.shaftCount; shaft++) {
            this.canvas.drawCharacter(this.offset.xWeft - 1, this.offset.yCenter - shaft, shaft);
        }

        for (var treadle = 1; treadle <= this.options.treadleCount; treadle++) {
            this.canvas.drawCharacter(this.offset.xCenter + treadle, this.offset.yWarp + 1, treadle);
        }
    }.bind(this);

    this.renderTreadling = function renderTreadling () {
        for (var row = 1; row <= this.options.drawdownHeight; row++) {
            var treadles = this.wif.data.treadling[((row - 1) % (this.wif.data.treadling.length - 1)) + 1];

            for (var treadle = 1; treadle <= this.options.treadleCount; treadle++) {
                this.canvas.drawBorderedBox(this.offset.xCenter + treadle, this.offset.yCenter + row,
                    COLOR_TABLE[treadles.includes(treadle) ? 1 : 2]);
            }
        }
    }.bind(this);

    this.renderWeft = function renderWeft () {
        for (var row = 1; row <= this.options.drawdownHeight; row++) {
            this.canvas.drawBorderedBox(this.offset.xWeft, this.offset.yCenter + row,
                this.wif.data.color_table[this.wif.data.weft_colors[1]]);
        }
    }.bind(this);

    this.renderDrawdown = function renderDrawdown () {
        var data = this.wif.data;
        var offset = this.offset;
        var currentFace = [];
        var previousFace = [];

        for (var row = 1; row <= this.options.drawdownHeight; row++) {

            var treadles = data.treadling[((row - 1) % (data.treadling.length - 1)) + 1];
            var shafts = treadles.reduce(function(current, value) { return current.concat(data.tieup[value]) }, []);

            for (var col = 1; col <= this.options.drawdownWidth; col++) {

                var isShowingWarp = data.threading[((col - 1) % (data.threading.length - 1)) + 1].reduce(
                    function (current, value) {
                        return current || shafts.includes(value)
                    }, false);
                currentFace[col] = isShowingWarp;

                var color;
                if (isShowingWarp) {
                    var warpColorIndex = ((col - 1) % (data.warp_colors.length - 1)) + 1;
                    color = data.color_table[data.warp_colors[warpColorIndex]];
                } else {
                    var weftColorIndex = ((row - 1) % (data.weft_colors.length - 1)) + 1;
                    color = data.color_table[data.weft_colors[weftColorIndex]];
                }

                this.canvas.drawBox(offset.xCenter - col, offset.yCenter + row, color);


                var showWarpDivider = col > 1
                    // previous thread was showing a different face to this one
                    && (currentFace[col] != currentFace[col - 1]
                        // showing warp and previous thread was also showing warp
                        || (currentFace[col] && currentFace[col - 1]));

                // On last row of pixels because previous line is displayed under this line
                var showWeftDivider = row > 1
                    && (currentFace[col] != previousFace[col]
                        // showing weft and previous line was also showing weft
                        || (!currentFace[col] && !previousFace[col]));

                this.canvas.drawBorder(offset.xCenter - col, offset.yCenter + row, { top: showWeftDivider, right: showWarpDivider});
            }

            previousFace = currentFace;
            currentFace = [];

            this.canvas.drawBorderAround(1, offset.yCenter + 1, this.options.drawdownWidth, this.options.drawdownHeight);
        }
    }.bind(this);

    this.render = function render () {
        this.renderTieup();

        this.renderWeft();
        this.renderWarp();

        this.renderTreadling();
        this.renderThreading();

        this.renderDrawdown();
    }.bind(this);

    function maxIn (arr_arr) {
        return arr_arr.reduce(function (current, values) {
            return Math.max(current, Math.max.apply(this, values));
        }, 0)
    }

    return this;
}
