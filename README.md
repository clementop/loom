# Loom

Static site content for generating woven patterns.
The loom will choose a random pattern from the embedded WIFs, or choose a random
pattern from a WIF file that has been uploaded.

Current displayed pattern can be downloaded as a WIF file.